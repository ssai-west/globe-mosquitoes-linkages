#!/usr/bin/env python
# coding: utf-8

# # Mosquito Linkages

# The idea behind this notebook is to find data records from a secondary protocol such as air temperature or precipitation that are close in time and space to records from the Mosquito Habitat Mapper.  

# ### Importing Required Modules

# A few Python modules and tools are required to run this script.

# In[1]:


# subroutine for designating a code block
def designate(title, section='main'):
    """Designate a code block with a title so that the code may be hidden and reopened.
    
    Arguments:
        title: str, title for code block
        section='main': str, section title
        
    Returns:
        None
    """
    
    # begin designation
    designation = ' ' * 20
    
    # if marked for user parameters
    if section == 'settings':
        
        # begin designation with indicator
        designation = '*** settings -----> '
    
    # add code designator
    designation += '^ [code] (for {}: {})'.format(section, title)
    
    # print
    print(designation)
    
    return None

# apply to itself
designate('designating hidden code blocks', 'designation')


# In[2]:


designate('importing Python system tools')

# import os and sys modules for system controls
import os
import sys

# set runtime warnings to ignore
import warnings

# import requests and json modules for making API requests
import requests
import json

# import fuzzywuzzy for fuzzy name matching
from fuzzywuzzy import fuzz

# import datetime module for manipulating date and time formats
from datetime import datetime, timedelta

# import pandas for dataframe manipulation
import pandas


# In[3]:


designate('importing Python mathematical modules')

# import numpy for math
from numpy import array, isnan
from numpy import exp, sqrt, log, log10, sign, abs
from numpy import arcsin, sin, cos, pi
from numpy import average, std, histogram, percentile
from numpy.random import random, choice

# import scipy for scientific computing
from scipy import stats
from scipy.optimize import curve_fit

# import sci-kit for linear regressions
from sklearn.neighbors import BallTree
from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import LinearRegression


# In[4]:


designate('importing Python visualization modules')

# import bokeh for plotting graphs
from bokeh.plotting import figure
from bokeh.io import output_notebook, show
from bokeh.layouts import row as Row
from bokeh.models import HoverTool, ColumnDataSource
from bokeh.models import Circle, LinearAxis, Range1d

# import ipyleaflet and branca for plotting maps
from ipyleaflet import Map, Marker, basemaps, CircleMarker, LayerGroup
from ipyleaflet import WidgetControl, ScaleControl, FullScreenControl, LayersControl
from branca.colormap import linear as Linear, StepColormap

# import iPython for javascript based notebook controls
from IPython.display import Javascript, display, FileLink

# import ipywidgets for additional widgets
from ipywidgets import Label, HTML, Button, Output, Box, VBox, HBox


# In[5]:


designate('inferring fuzzy match', 'tools')

# subroutine for fuzzy matching
def infer(text, options):
    """Infer closest match of text from a list of options.
    
    Arguments:
        text: str, entered text
        options: list of str, the options
        
    Returns:
        str, the closest match
    """
    
    # perform fuzzy search to get closest match
    fuzzies = [(option, fuzz.ratio(text, option)) for option in options]
    fuzzies.sort(key=lambda pair: pair[1], reverse=True)
    inference = fuzzies[0][0]
    
    return inference


# In[6]:


designate('truncating field names', 'tools')

# truncate field names to first capital
def truncate(name, size=5, minimum=4, maximum=15):
    """Truncate a name to the first captial letter past the minimum.
    
    Arguments:
        name: str, the name for truncation
        size: the final size of the truncation
        minimum=4: int, minimum length of name
        maximum=15: int, maximum length of name
        
    Returns
        str, truncated name
    """
    
    # chop name at maximum and capitalize
    name = name[-maximum:]
    name = name[0].capitalize() + name[1:]
    
    # make stub starting at minimum length
    length = minimum
    stub = name[-length:]
    while not stub[0].isupper():
        
        # add to length
        length += 1
        stub = name[-length:]
        
    # only pass size
    stub = stub[:size]
        
    return stub


# In[7]:


designate('entitling a name by capitalizing', 'tools')

# entitle function to capitalize a word for a title
def entitle(word):
    """Entitle a word by capitalizing the first letter.
    
    Arguments:
        word: str
        
    Returns:
        str
    """
    
    # capitalize first letter
    word = word[0].upper() + word[1:]
    
    return word


# In[8]:


designate('resolving country name and code', 'tools')

# resolving country name and codes
def resolve(country, code):
    """Resolve the country code from given information.
    
    Arguments:
        country: str, country name as input
        code: str, country code as input
        
    Returns:
        (str, str) tuple, the country name and country code
    """
    
    # check for code
    if code:
        
        # find closest matching code
        code = infer(code, [member for member in codes.values()])
        country = countries[code]
    
    # if no code, but a country is given
    if not code and country:
        
        # find closest matching country
        country = infer(country, [member for member in codes.keys()])
        code = codes[country]
    
    # if there's no code, check the country
    if not code and not country:
        
        # default to all countries
        country = 'All countries'
        code = ''
    
    return country, code


# In[9]:


designate('scanning notebook for cells', 'introspection')

# scan notebook for cell information
def scan():
    """Scan the notebook and collect cell information.

    Arguments:
        None

    Returns:
        list of dicts
    """

    # open the notebook file 
    with open('linkages.ipynb', 'r', encoding='utf-8') as pointer:
        
        # and read its contents
        contents = json.loads(pointer.read())

    # get all cells
    cells = contents['cells']

    return cells


# In[10]:


designate('notching ticks in a range', 'graphing')

# notch function to get evenly spaced points in a range
def notch(left, right, number=100):
    """Notch a number of ticks along a span
    
    Arguments:
        left: float, left axis boundary
        right: float, right axis boundary
        number=100: number ticks
        
    Returns:
        list of floats
    """
    
    # get chunk length
    chunk = (right - left) / number
    ticks = [left + index * chunk for index in range(number + 1)]
    
    return ticks


# In[11]:


designate('sketching functions', 'graphing')

# sketch a function
def sketch(*functions, legend=None, span=(-5, 5)):
    """Sketch a function.
    
    Arguments:
        *functions: unpacked list of function objects
        legend=None: list of str
        span=(-5, 5): tuple of floats, the x-axis range
        
    Returns:
        None
    """
    
    # begin curve
    curve = figure(x_range=span, plot_width=300, plot_height=300)
    
    # set colors
    colors = ['red', 'green', 'blue', 'violet', 'cyan', 'orange']
    
    # set default legend
    if not legend:
        
        # set up legend
        legend = [str(index + 1) for index, _ in enumerate(functions)]
    
    # get points
    xs = notch(*span)
    
    # plot functions
    for function, color, name in zip(functions, colors, legend):
        
        # graph line
        points = [{'x': x, 'y': function(x)} for x in xs]
        table = ColumnDataSource(pandas.DataFrame(points))
        curve.line(source=table, x='x', y='y', color=color, line_width=1, legend_label=name)
        
    # add hover annotations and legend
    annotations = [('x', '@x'), ('y', '@y')]
    hover = HoverTool(tooltips=annotations)
    curve.add_tools(hover)
    curve.legend.location='top_left'
    
    # show the results
    output_notebook()
    show(curve)
    
    return None


# In[12]:


designate('annotating graphs', 'graphing')

# annotate graphs subroutine
def annotate(graph, annotations):
    """Annotate the graph with annotations.
    
    Arguments:
        graph: bokeh graph object
        annotations: list of (str, str) tuples
        
    Returns:
        graph object
    """
    
    # set up hover summary
    summary = """
    <style>
        .bk-tooltip>div:not(:first-child) {display:none;}
    </style>
    
    """
    
    # add annotations
    for field, value in annotations:
        
        # add to summary
        summary += '<b>{}: </b> {} <br>'.format(field, value)

    # setup hovertool
    hover = HoverTool()
    hover.tooltips = summary
    graph.add_tools(hover)
            
    # set up graph legend
    graph.legend.location='top_left'
    graph.legend.click_policy='hide'
    
    return graph


# In[13]:


designate('defining global variables')

# ignore runtime warnings
warnings.filterwarnings('ignore')

# set pandas optinos
pandas.set_option("display.max_rows", None)
pandas.set_option("display.max_columns", None)

# begin optimizations list for previous optimizations
optimizations = []

# establish genera and colors
classification = ['Unknown', 'Other', 'Aedes', 'Anopheles', 'Culex']
colors = ['gray', 'green', 'crimson', 'orange', 'magenta']

# create indicator colors to be used on plots
indicators = {genus: color for genus, color in zip(classification, colors)}
indicators.update({'All': 'blue'})

# initiate regression modes
regressions = {mode: {} for mode in ('linear', 'quadratic', 'exponential', 'power', 'logistic', 'gaussian')}

# define cancellation message
cancellation = 'no fit achieved'

# initialize memory dictionary for latitude, longitude measurements
memory = {}

# define template for units
template = {'distance': '(km)', 'interval': '(d)', 'lag': '(d)', 'confidence': '', 'cutoff': '', 'inclusion': ''}
template.update({'mode': '', 'genus': '', 'records': '', 'pairs': '', 'coverage': ''})
template.update({'error': '(larvae)', 'pearson': '(r)', 'fit': '(r^2)', 'pvalue': '', 'equation': ''})
template.update({'slope': '(larvae/feature)', 'center': '(feature)', 'onset': '(feature)'})
template.update({'curvature': '(larvae/feature^2)', 'height': '(larvae)', 'rate': '(/feature)'})
template.update({'power': '', 'spread': '(feature^2)'})

# define units
making = lambda unit: lambda feature: unit.replace('feature', truncate(feature))
units = {field: making(unit) for field, unit in template.items()}

# make doppelganger for navigation
doppelganger = scan()


# In[14]:


designate('import status')

# print status
print('modules imported.')


# ### Notes on Navigation

# General Organization:
# 
# 
# - The notebook is organized in two main sections, with documentation near the top and user settings and plots in the second half.  Relevant subroutines are generally presented in the documentation sections, or at the end of the preceding section.
# 
# 
# - There are several sections that require the input of user selected parameters.  Click Apply to see the affect of changing those parameters on that section only, then Propagate to propagate the changes down the notebook.  Clicking Both will do both these actions.
# 
# Running Cells:
# 
# 
# - Upon loading the notebook, most plots will not be visible.  It is necessary to run all the code by selecting "Restart & Run All" from the "Kernel" menu and clicking "Restart and Run All Cells" to confirm.
# 
# 
# - This action may be performed at any time, for instance after altering the parameters or changing the code in other ways.
# 
# 
# - Alternatively, any single block of code may be rerun by highlighting the block and pressing Shift-Return.
# 
# 
# - Also, under the "Cell" menu is the option to "Run All Below" the currently selected cell, or to simply "Run Cells" that have been selected.
# 
# 
# Processing Indicator:
# 
# - In the upper righthand corner it says "Python 3" with a circle.  If this circle is black, it means the program is still processing.   A hollow circle indicates all processing is done.
# 
# 
# Collapsible Headings and Code Blocks:
# 
# - The Jupyter notebook format features collapsible code sections and headings.  An entire section may be collapsed by clicking on the downward pointing triangle at the left of the heading.  
# 
# 
# - Likewise, blocks of code are loaded hidden from view, and designated with '[code] ^'.  Click on the '[code] ^' text and select '^' from the toolbar next to "Download" to expand the code.  Blocks with user parameters to enter are marked with *** settings ---->.
# 
# 
# - All code blocks may be hidden or exposed by toggling the eye icon in the toolbar.
# 
# 
# - Large output boxes may be collapsed to scrollable window by clicking to the left, and may also be collapsed completely by double-clicking in the same area.  Clicking on the "..." will reopen the area.
# 
# 
# Hosting by myBinder:
# 
# 
# - This notebook is hosted by myBinder.org in order to maintain its interactivity within a browser without the user needing an established Python environment.  Unfortunately, connection with myBinder.org will break after 10 minutes of inactivity.  In order to reconnect you may use the link under "Browser Link" to reload.
# 
# 
# - The state of the notebook may be saved by clicking the leftmost cloud icon in the toolbar to the right of the Download button.  This saves the notebook to the browser.  The rightmost cloud icon can then retrieve this saved state in a newly opened copy.  Often reopening a saved version comes with all code blocks visible, so toggle this using the eye icon in the toolbar.
# 
# 
# - The following browser link will reload the notebook in case the connection is lost:
# https://mybinder.org/v2/git/https%3A%2F%2Fmattbandel%40bitbucket.org%2Fmattbandel%2Fglobe-mosquitoes-linkages.git/master?filepath=linkages.ipynb

# In[15]:


designate('looking for particular cell', 'navigation')

# function to look for cells with a particular text snippet
def look(text):
    """Look for a particular text amongst the cells.
    
    Arguments:
        text: str, the text to search for
        
    Returns:
        list of int, the cell indices.
    """
    
    # search for cells 
    indices = []
    for index, cell in enumerate(doppelganger):
        
        # search for text in source
        if any([text in line.replace("'{}'".format(text), '') for line in cell['source']]):
            
            # add to list
            indices.append(index)
            
    return indices


# In[16]:


designate('jumping to a particular cell', 'navigation')

# jump to a particular cell
def jump(identifier):
    """Jump to a particular cell.
    
    Arguments:
        identifier: int or str
        
    Returns:
        None
    """
    
    # try to look for a string
    try:
        
        # assuming string, take first index with string
        index = look(identifier)
        
    # otherwise assume int
    except (TypeError, IndexError):
        
        # index is identifier
        index = identifier 
    
    # scroll to cell
    command = 'IPython.notebook.scroll_to_cell({})'.format(index)
    display(Javascript(command))
    
    return


# In[17]:


designate('executing cell range by text', 'navigation')

# execute cell range command
def execute(start, finish):
    """Execute a cell range based on text snippets.
    
    Arguments:
        start: str, text from beginning cell of range
        finish: str, text from ending cell of range
        
    Returns:
        None
    """
    
    # find start and finish indices, adding 1 to be inclusive
    opening = look(start)[0] 
    closing = look(finish)[0]
    bracket = (opening, closing)
    
    # make command
    command = 'IPython.notebook.execute_cell_range' + str(bracket)
    
    # perform execution
    display(Javascript(command))
    
    return None


# In[18]:


designate('refreshing cells by relative position', 'navigation')

# execute cell range command
def refresh(start, finish=None):
    """Refresh a particular cell relative to current cell.
    
    Arguments:
        start: int, the first cell offset
        finish=None: int, the second cell offset
        
    Returns:
        None
    """
    
    # make offset into a string
    stringify = lambda offset: str(offset) if offset < 0 else '+' + str(offset)
    
    # default finish to start
    finish = finish or start
    
    # make command
    command = 'IPython.notebook.execute_cell_range('
    command += 'IPython.notebook.get_selected_index()' + stringify(start) + ','
    command += 'IPython.notebook.get_selected_index()' + stringify(finish + 1) + ')'
    
    # perform execution
    display(Javascript(command))
    
    return None


# In[19]:


designate('revealing open cells', 'navigation')

# outline headers
def reveal(cells):
    """Outline the headers and collapsed or uncollapsed state.
    
    Arguments:
        cells: dict
        
    Returns:
        list of int, the indices of visible cells
    """
    
    # search through all cells for headers
    indices = []
    visible = True
    for index, cell in enumerate(cells):
        
        # check for text
        header = False
        if any(['###' in text for text in cell['source']]):
        
            # check for header and visible state
            header = True
            visible = True
            if 'heading_collapsed' in cell['metadata'].keys(): 

                # set visible flag
                visible = not cell['metadata']['heading_collapsed']

        # if either header or visible
        if header or visible: 

            # add to indices
            indices.append(index)
            
    return indices


# In[20]:


designate('gauging cell size', 'navigation')

# measure a cell's line count and graphics
def gauge(cell):
    """Gauge a cell's line count and graphic size.
    
    Arguments:
        cell: cell dict
        
    Returns:
        (int, boolean) tuple, line count and graphic boolean
    """
    
    # check for display data
    graphic = False
    displays = [entry for entry in cell.setdefault('outputs', []) if entry['output_type'] == 'display_data']
    if len(displays) > 0:
        
        # check for many displays or one long one
        if len(displays) > 2 or '…' in displays[0]['data']['text/plain'][0]:

            # switch graphic to true
            graphic = True

    # determine total lines of text in source, 2 by default
    length = 2
    
    # determine executions
    executions = [entry for entry in cell.setdefault('outputs', []) if entry['output_type'] == 'execute_result']
    for execution in executions:
        
        # add to length
        length += execution['execution_count']
    
    # check hide-input state
    if not cell['metadata'].setdefault('hide_input', False):
        
        # add lines to source
        source = cell['source']
        for line in source:

            # split on newlines
            length += sum([int(len(line) / 100) + 1 for line in line.split('\n')])

    return length, graphic


# In[21]:


designate('bookmarking cells for screenshotting', 'navigation')

# bookmark which cells to scroll to
def bookmark(cells):
    """Bookmark which cells to scroll to.

    Arguments:
        cells: list of dicts
        visibles: list of ints

    Returns:
        list of ints
    """

    # set page length criterion and initialize counters
    criterion = 15
    accumulation = criterion + 1

    # determine scroll indices
    bookmarks = []
    visibles = reveal(cells)
    for index in visibles:

        # measure cell and add to total
        cell = cells[index]
        length, graphic = gauge(cell)
        accumulation += length
        
        # compare to criterion
        if accumulation > criterion or graphic:

            # add to scrolls and reset
            bookmarks.append(index)
            accumulation = length

            # for a graphic, make sure accumulation is already maxed
            if graphic:
                
                # add to accumulation
                accumulation = criterion + 1

    return bookmarks


# In[22]:


designate('describing cell contents', 'navigation')

# describe the cells
def describe(*numbers):
    """Describe the list of cells by printing cell summaries.

    Arguments:
        numbers: unpacked list of ints

    Returns:
        None
    """

    # get cells and analyze
    visibles = reveal(doppelganger)
    bookmarks = bookmark(doppelganger)

    # print cell metadata
    for index, cell in enumerate(doppelganger):

        # construct stars to mark visible and bookmark statuses
        stars = '' + '*' * (int(index in visibles) + int(index in bookmarks))

        # check in numbers
        if len(numbers) < 1 or index in numbers:
        
            # print metadata
            print(' \n{} cell {}:'.format(stars, index))
            print(cell['cell_type'])
            print(cell['source'][0][:100])
            print(cell['metadata'])
            print([key for key in cell.keys()])
            if 'outputs' in cell.keys():

                # print outputs
                print('outputs:')
                for entry in cell['outputs']:

                    # print keys
                    print('\t {}, {}'.format(entry['output_type'], [key for key in entry.keys()]))

    return None


# In[23]:


designate('propagating setting changes across cells', 'buttons')

# def propagate
def propagate(start, finish, finishii, descriptions=['Apply', 'Propagate', 'Both']):
    """Propagate changes across all code cells given by the headings.
    
    Arguments:
        start: str, top header
        finish: str, update stopping point
        finishii: str, propagate stopping point
        descriptions: list of str
        
    Returns:
        None
    """
    
    # define jump points
    cues = [(start, finish), (finish, finishii), (start, finishii)]
    
    # make buttons
    buttons = []
    buttoning = lambda start, finish: lambda _: execute(start, finish)
    for description, cue in zip(descriptions, cues):

        # make button
        button = Button(description=description)
        button.on_click(buttoning(*cue))
        buttons.append(button)

    # display
    display(HBox(buttons))
    
    return None


# In[24]:


designate('navigating to main sections', 'buttons')

# present buttons to jump to particular parts of the notebook
def navigate():
    """Guide the user towards regression sections with buttons.
    
    Arguments:
        None
        
    Returns:
        None
    """

    # define jump points
    descriptions = ['Top', 'Settings', 'Filter', 'Weights', 'Data', 'Map']
    cues = ['# Mosquitoe Larvae Regressions', '### Setting the Parameters', '### Filtering Records']
    cues += ['### Defining the Weighting Scheme', '### Viewing the Data Table', '### Visualizing on a Map', ]
    
    # make buttons
    buttons = []
    buttoning = lambda cue: lambda _: jump(cue)
    for description, cue in zip(descriptions, cues):

        # make button
        button = Button(description=description)
        button.on_click(buttoning(cue))
        buttons.append(button)

    # display
    display(HBox(buttons))
    
    return None


# In[25]:


designate('guiding to regression modes', 'buttons')

# present buttons to choose the regression part of the notebook
def guide():
    """Guide the user towards regression sections with buttons.
    
    Arguments:
        None
        
    Returns:
        None
    """

    # make buttons
    buttons = []
    buttoning = lambda mode: lambda _: jump('### ' + mode.capitalize() + ' Regression')
    for mode in regressions.keys():

        # make button
        button = Button(description=mode.capitalize())
        button.on_click(buttoning(mode))
        buttons.append(button)

    # display
    display(HBox(buttons))
    
    return None


# ### Getting Started

# - Select "Restart & Run All" from the Kernel menu, and confirm by clicking on "Restart and Run All Cells" and wait for the processing to stop (the black circle in the upper right corner next to "Python 3" will turn hollow).
# 
# 
# - Use a Settings button from a navigation menu like the one below to navigate to the Settings section.
# 
# 
# - Find the ^ [code] block marked with *** setings ----->, and open it using the "^" button in the toolbar at the top of the page.  Begin inputting your settings and apply the changes with the buttons.  Then move on to the next section and apply those settings as well.

# In[26]:


designate('navigation buttons')

# set two navigation buttons
navigate()


# ### Data Preparation

# The process begins with the call to the GLOBE API:

# https://www.globe.gov/en/globe-data/globe-api

# In[27]:


designate('calling the api', 'api')

# call the api with protocol and country code
def call(protocol, code, beginning, ending, sample=False):
    """Call the api:
    
    Arguments:
        protocol: str, the protocol
        code: str, the country code
        beginning: str, the beginning date
        ending: str, the ending date
        sample=False: boolean, only get small sampling?
        
    Returns:
        list of dicts, the records
    """
    
    # default to all countries unless a code is specified
    extension = 'country/' if code else ''
    extensionii = '&countrycode=' + code if code else ''
    
    # assemble the url for the API call 
    url = 'https://api.globe.gov/search/v1/measurement/protocol/measureddate/' + extension
    url += '?protocols=' + protocol
    url += '&startdate=' + beginning 
    url += '&enddate=' + ending
    url += extensionii

    # geojson parameter toggles between formats
    url += '&geojson=FALSE'
    
    # sample parameter returns small sample set if true
    url += '&sample=' + str(sample).upper()

    # make the API call and return the raw results
    request = requests.get(url)
    raw = json.loads(request.text)
    
    return raw


# After retrieving the data, several steps are taken to prepare the data.  Initially, the data is returned in a nested structure.  It is useful to flatten this nesting so that all fields are readily accessible.  

# In[28]:


designate('flattening records', 'processing')

# function to flatten a nested list into a single-level structure
def flatten(record, label=None):
    """Flatten each record into a single level.

    Arguments:
        record: dict, a record
        label: str, key from last nesting

    Returns:
        dict
    """

    # initiate dictionary
    flattened = {}

    # try to flatten the record
    try:

        # go through each field
        for field, info in record.items():

            # and flatten the smaller records found there
            flattened.update(flatten(info, field))

    # otherwise record is a terminal entry
    except AttributeError:

        # so update the dictionary with the record
        flattened.update({label: record})

    return flattened


# Additionally, it can be useful to abbreviate the fields of interest as the initial field names are often quite long.

# In[29]:


designate('abbreviating records', 'processing')

# function to abbreviate the fields of a record
def abbreviate(record, primary=True, removal=True):
    """Abbreviate certain fields in the record for easier manipulation later.
    
    Arguments:
        record: dict
        primary=True: boolean, primary record?
        removal=True: boolean, remove original fields?
        
    Returns:
        dict
    """
    
    # define abbreviations dictionary for primary records
    abbreviations = {}
    abbreviations['count'] = larvae
    abbreviations['genus'] = 'mosquitohabitatmapperGenus'
    abbreviations['source'] = 'mosquitohabitatmapperWaterSource'
    abbreviations['stage'] = 'mosquitohabitatmapperLastIdentifyStage'
    abbreviations['type'] = 'mosquitohabitatmapperWaterSourceType'
    abbreviations['measured'] = 'mosquitohabitatmapperMeasuredAt'
    abbreviations['habitat'] = 'mosquitohabitatmapperWaterSourcePhotoUrls'
    abbreviations['body'] = 'mosquitohabitatmapperLarvaFullBodyPhotoUrls'
    abbreviations['abdomen'] = 'mosquitohabitatmapperAbdomenCloseupPhotoUrls'
    
    # if a secondary record
    if not primary:

        # define abbreviations dictionary for secondary protocol
        abbreviations = {}
        abbreviations['feature'] = feature
        abbreviations['measured'] = measured

    # and each abbreviation
    for abbreviation, field in abbreviations.items():

        # copy new field from old, or None if nonexistent
        record[abbreviation] = record.setdefault(field, None)
        
        # remove original field if desired
        if removal:
            
            # remove field
            del(record[field])
    
    return record


# As all measurements are recorded in reference to UTC time, it is helpful to convert the measurements to local times.  This is accomplished by adjusting the hour according to the longitude.  Though this may not accurately reflect the local time in a political sense as it ignores daylight savings time and time zone boundaries, it is perhaps a more accurate measure in the astronomical sense.

# In[30]:


designate('synchronizing times with longitudes', 'processing')

# synchronize the time of measurement with the longitude for local time
def synchronize(record):
    """Synchronize the measured times with longitudes.
    
    Arguments:
        record: dict
        
    Returns:
        dict
    """

    # convert the date string to date object and normalize based on partitioning
    record['time'] = datetime.strptime(record['measured'], "%Y-%m-%dT%H:%M:%S")
    record['date'] = record['time'].date()
    
    # convert the date string to date object and correct for longitude
    zone = int(round(record['longitude'] * 24 / 360, 0))
    record['hour'] = record['time'] + timedelta(hours=zone)

    return record


# The larvae count data are initially returned as strings.  In order to analyze the data, these strings must be converted into numbers.  Additionally, some of the data is entered as a range (e.g., '1-25'), or as a more complicated string ('more than 100').  These strings will be converted to floats using the following rules:
# - a string such as '50' is converted to its floating point equivalent (50)
# - a range such as '1-25'is converted to its average (13)
# - a more complicated string, such as 'more than 100' is converted to its nearest number (100)

# In[31]:


designate('converting strings to numbers', 'processing')

# function to convert a string into a floating point number
def convert(record, field, name):
    """Translate info given as a string or range of numbers into a numerical type.
    
    Arguments:
        record: dict
        field: str, the field to get converted
        name: str, the name of the new field
        
    Returns:
        float
    """
    
    # try to convert directly
    info = record[field]
    try:
        
        # translate to float
        conversion = float(info)
        
    # otherwise
    except ValueError:
        
        # try to convert a range of values to their average
        try:
        
            # take the average, assuming a range separated by a hyphen
            first, last = info.split('-')
            first = float(first.strip())
            last = float(last.strip())
            conversion = float(first + last) / 2
            
        # otherwise
        except ValueError:
            
            # scan for digits
            digits = [character for character in info if character.isdigit()]
            conversion = ''.join(digits)
            conversion = float(conversion)
            
    
    # add new field
    record[name] = conversion
        
    return record


# Also, some steps have been taken towards mosquito genus identification.  The three noteworthy genera in terms of potentially carrying diseases are Aedes, Anopheles, and Culex.  If the identification process did not lead to one of these three genera, the genus is regarded as "Other."  If the identification process was not fully carried out, the genus is regarded as "Unknown."

# In[32]:


designate('identifying mosquito genera', 'processing')

# function to identify the mosquito genera based on last stage of identification
def identify(record):
    """Identify the genera from a record.
    
    Arguments:
        record: dict
        
    Returns:
        dict
    """

    # check genus
    if record['genus'] is None:

        # check last stage
        if record['stage'] in (None, 'identify'):

            # correct genus to 'Unidentified'
            record['genus'] = 'Unknown'

        # otherwise
        else:

            # correct genus to 'Other'
            record['genus'] = 'Other'
                
    return record


# Also, many of the records contain photo urls.  These will be parsed and the file names formatted according to a naming convention.

# In[33]:


designate('localizing latitude and longitude', 'processing')
    
# specify the location code for the photo based on its geo coordinates
def localize(latitude, longitude):
    """Specify the location code for the photo naming convention.
    
    Arguments:
        latitude: float, the latitude
        longitude: float, the longitude
        
    Returns:
        str, the latlon code
    """
    
    # get latlon codes based on < 0 query
    latitudes = {True: 'S', False: 'N'}
    longitudes = {True: 'W', False: 'E'}
    
    # make latlon code from letter and rounded geocoordinate with 3 places
    latlon = latitudes[latitude < 0] + ('000' + str(abs(int(latitude))))[-3:]
    latlon += longitudes[longitude < 0] + ('000' + str(abs(int(longitude))))[-3:]
    
    return latlon


# In[34]:


designate('applying photo naming convention', 'processing')

# apply the naming convention to a photo url to make a file name
def apply(urls, code, latitude, longitude, time):
    """Apply the naming convention to a group of urls
    
    Arguments:
        urls: list of str, the photo urls
        code: str, the photo sector code
        latitude: float, the latitude
        longitude: float, the longitude
        time: datetime object, the measurement time
        
    Returns:
        list of str, the filenames
    """
    
    # begin file name with protocol and latlon
    base = 'GLOBEMHM_' + localize(latitude, longitude) + '_'
    
    # add the measurement time and sector code
    base += time.strftime('%Y%m%dT%H%MZ') + '_' + code
    
    # add index and unique id
    names = []
    for index, url in enumerate(urls):
        
        # add index, starting with 1
        name = base + str(index + 1)
            
        # add unique id and extension
        unique = url.split('/')[-2]
        name += '_' + unique + '.jpg'
        names.append(name)
        
    return names


# In[35]:


designate('parsing photo urls', 'processing')

# function for parsing photo urls
def parse(record):
    """Parse photo url information.
    
    Arguments:
        record: dict
        
    Returns:
        dict
    """

    # dictionary of photo sector codes
    sectors = {'habitat': 'WS', 'body': 'FB', 'abdomen': 'AB'}

    # initialize fields for each sector and parse urls
    record['originals'] = []
    record['thumbs'] = []
    record['photos'] = []
    for field, stub in sectors.items():
        
        # split on semicolon, and keep all fragments with 'original'
        datum = record[field] or ''
        originals = [url.strip() for url in datum.split(';') if 'original' in url]
        
        # sort by the unique identifier as the number before the last slash
        originals.sort(key=lambda url: url.split('/')[-2])
        record['originals'] += originals
        
        # get the thumbnail versions
        thumbs = [url.split('original')[0] + 'small.jpg' for url in originals]
        record['thumbs'] += thumbs
        
        # apply the naming convention
        photos = apply(originals, code, record['latitude'], record['longitude'], record['time'])
        record['photos'] += photos
        
    return record


# In[36]:


designate('processing records', 'processing')

# function for processing records
def process(records, primary=True):
    """Process all records.
    
    Arguments:
        records: list of dicts
        primary=True: boolean, primary record?
        
    Returns:
        list of dicts
    """
    
    # flatten and abbreviate all records
    records = [flatten(record) for record in records]
    records = [abbreviate(record, primary) for record in records]
    records = [synchronize(record) for record in records]
    
    # process primary records
    if primary:
        
        # process
        records = [convert(record, 'count', 'larvae') for record in records]
        records = [identify(record) for record in records]
        records = [parse(record) for record in records]
        
    # process secondary records
    if not primary:
        
        # process
        records = [convert(record, 'feature', feature) for record in records]
        
    return records


# FInally, it is sometimes the case that records contain suspicous data.  For instance, an entry of '1000000' for larvae counts is suspicous because likely no one counted one million larvae.  These data can skew analysis and dwarf the rest of the data in graphs.  
# 
# The approach taken here is to calculate a "z-score" for each record:
# 
# $z_i=\dfrac{(x_i-\mu)}{\sigma}$
# 
# where:
# 
# - $x_i$ is the particular observation
# - $\mu$ is the mean of all observations
# - $\sigma$ is the standard deviation
# 
# The z-score measures how many standard deviations the observation is from the mean of all observations.  A highly negative or positive z-score, for instance 20, indicates an observation 20 standard deviations away from the mean.  In a normal distribution, 99% of observations are found within 3 standard deviations, so an abnormally high z-score indicates a highly unlikely observation.  
# 
# Observations are pruned recursively.  For instance, a highly outlying observation skews its own mean and standard deviation.  Removing the outlier creates a new distribution with different mean and standard deviation.  Therefore, an observation that was within the z-score threshold before removing the outlier might be beyond the threshold once the outlier is removed.  Therefore, the pruning calculations are repeated until no more outliers get removed.

# In[37]:


designate('navigation buttons')

# set two navigation buttons
navigate()


# In[38]:


designate('outlier pruning', 'pruning')

# function to prune away outlying observations
def prune(records, field, threshold):
    """Prune away outlying observations based on a threshold z-score.
    
    Arguments:
        records: list of dicts, the records
        field: str, field under inspection
        threshold: float, z-score threshold
        
    Returns:
        tuple of two lists of dicts, (pruned records, outliers)
    """

    # continually attempt pruning until there are no more records removed, but at least two remain
    outliers = []
    number = len(records) + 1
    while len(records) < number and len(records) > 2:
        
        # reset number of records
        number = len(records)
        
        # calculate the mean and standard deviation
        values = [record[field] for record in records]
        mean = average(values)
        deviation = std(values)

        # for each record
        for record in records:

            # calculate the z-score as the x - mean / standard deviation
            record['score'] = abs((record[field] - mean) / deviation)

            # if the threshold is exceeded
            if record['score'] >= threshold:

                # append to outliers
                outliers.append(record)
            
        # only retain records with z-scores below the threshold
        records = [record for record in records if record['score'] < threshold]
        
    return records, outliers


# In[39]:


designate('sifting data through filter', 'filtering')

# function to sift data through filters
def sift(records, parameters, fields, functions, symbols):
    """Sift records according to parameters.
    
    Arguments:
        records: list of dicts
        parameters: list of settings
        fields: list of str
        functions: list of function objects
        symbols: list of str
        
    Returns:
        list of dicts, str
    """
    
    # begin criteria string
    criteria = ''

    # filter primaries based on parameters
    for parameter, field, function, symbol in zip(parameters, fields, functions, symbols):

        # check for None
        if parameter is not None:

            # filter
            if field in records[0].keys():
            
                # filter
                records = [record for record in records if function(record[field], parameter)]

                # add to criteria string
                criteria += '{} {} {}\n'.format(field, symbol, parameter)
                
    # sort data by date and add an index
    records.sort(key=lambda record: record['date'])
    [record.update({'index': index}) for index, record in enumerate(records)]

    return records, criteria


# In[40]:


designate('chopping data up into bins', 'histograms')

# chopping data into histogram bars
def chop(observations, width=1, limit=1000):
    """Chop the observations from the records up into bars
    
    Arguments:
        observations: list of floats
        width=1: float, minimum width of each bar
        limit: int, maximum number of bins
        
    Returns:
        (float, float) tuple, the number of bins and the width
    """

    # adjust width until number of bins is less than a limit
    bins = limit + 1
    width = width / 10
    while bins > limit:
    
        # multiply width by 10
        width *= 10
    
        # calculate the number of histogram bins
        minimum = (int(min(observations) / width) * width) - (width * 0.5)
        maximum = (int(max(observations) / width) * (width + 1)) + (width * 0.5)
        bins = int((max(observations) - min(observations)) / width) + 1

    # readjust maximum to cover an even number of bins
    maximum = minimum + bins * width

    return bins, width, minimum, maximum


# In[41]:


designate('zooming in on best view', 'histograms')

# function to define horizontal and vertical ranges of the graph
def zoom(observations, counts, width, percent=1):
    """Zoom in on the best horizontal and vertical view ranges.
    
    Arguments:
        observations: list of float
        counts: list of counts per bin
        width: width of each bin
        percent: float, the percentile margin
        
    Returns:
        tuple of tuples of floats, the view boundaries
    """
    
    # make left and right boudaries as a width past the percentiles
    left = percentile(observations, percent) - width
    right = percentile(observations, 100 - percent) + width
    horizontal = (left, right)
    
    # make up and down boudaries based on counts
    down = 0
    up = max(counts) * 1.1
    vertical = (down, up)
    
    return horizontal, vertical


# In[42]:


designate('begin drafting a histogram', 'histograms')

# function for drafting a histogram
def draft(field, horizontal, vertical, mean, deviation):
    """Draft a histogram with beginning boundary information.
    
    Arguments:
        field: str
        horizontal: (float, float) tuple, the horizontal extent
        vertical: (float, float) tuple, the vertical extent
        mean: float, the mean of the observations
        deviation: standard deviation of the observations
        
    Returns:
        bokeh figure object
    """
    
    # create parameters dictionary for histogram labels
    parameters = {}
    parameters['title'] = 'Histogram for {}'.format(entitle(field))
    parameters['x_axis_label'] = '{}'.format(field)
    parameters['y_axis_label'] = 'observations'
    parameters['x_range'] = horizontal
    parameters['y_range'] = vertical
    parameters['plot_height'] = 400
    parameters['plot_width'] = 450
    
    # set extra axis
    starting = (horizontal[0] - mean) / deviation
    ending = (horizontal[1] - mean) / deviation
    parameters['extra_x_ranges'] = {'z-score': Range1d(start=starting, end=ending)}
    
    # initialize the bokeh graph with the parameters
    gram = figure(**parameters)
                                                       
    # label the histogram
    formats = round(mean, 2), round(deviation, 2)
    label = 'z-score of equivalent normal distribution (mean={}, std={})'.format(*formats)
    gram.add_layout(LinearAxis(x_range_name='z-score', axis_label=label), 'above')
    
    # add annotations
    annotations = [('{}:'.format(truncate(field, 6)), '@left to @right')]
    annotations += [('Observations:', '@ys')]
    annotations += [('Z-score:', '@scores')]
    
    # activate the hover tool
    hover = HoverTool(tooltips=annotations)
    gram.add_tools(hover)
    
    return gram


# In[43]:


designate('blocking in bars on the histogram', 'histograms')

# function to block in bars on the histogram
def block(gram, counts, edges, mean, deviation):
    """Block in bars on the histogram.
    
    Arguments:
        gram: bokeh figure
        counts: list of floats, the bin counts
        edges: list of floats, the bin edges
        mean: float
        deviation: float
        
    Returns:
        bokeh figure
    """
    
    # get middle points
    middles = [(right + left) / 2 for left, right in zip(edges[:-1], edges[1:])]
    
    # calculate z-scores for all middles
    scores = [(middle - mean) / deviation for middle in middles]
    
    # accumulate the info into a table
    table = {'ys': counts, 'left': edges[:-1], 'right': edges[1:]}
    table.update({'scores': scores, 'xs': middles})
    table = ColumnDataSource(table)
    
    # set parameters for drawing the bars, indicating the source of the data
    bars = {'source': table}
    bars.update({'left': 'left', 'right': 'right', 'bottom': 0, 'top': 'ys'})
    bars.update({'line_color': 'white', 'fill_color': 'lightgreen'})
    
    # add to histogram
    gram.quad(**bars)
    
    return gram


# In[44]:


designate('normalizing observations to a normal distribution', 'histograms')

# function to produce the normalization curve
def normalize(gram, counts, edges, mean, deviation):
    """Normalize the observations by drawing the normal distribution.
    
    Arguments:
        gram: bokeh figure
        counts: list of floats, the bin counts
        edges: list of floats, the bin edges
        mean: float
        deviation: float
        
    Returns:
        bokeh figure
    """
    
    # create line from z-score of -4 to 4
    scores = [tick * 0.01 - 4.0 for tick in range(801)]
    xs = [(score * deviation) + mean for score in scores]
    
    # create gaussian fucntion
    area = sum([count * (right - left) for count, left, right in zip(counts, edges[:-1], edges[1:])])
    height = area / (deviation * sqrt(2 * pi))
    normalizing = lambda x: height * exp(-(x - mean) ** 2 / (2 * deviation ** 2))
    ys = [normalizing(x) for x in xs]
    ys = [round(y, 3) for y in ys]

    # make column object
    table = ColumnDataSource({'xs': xs, 'ys': ys, 'scores': scores, 'left': xs, 'right': xs})
    summary = 'Normal Distribution'
    gram.line(source=table, x='xs', y='ys', color='blue')
    
    # draw standard lines
    for score in (-3, -2, -1, 0, 1, 2, 3):
        
        # draw std lines
        xs = [(deviation * score) + mean] * 2
        ys = [0, normalizing(xs[0])]
        table = ColumnDataSource({'xs': xs, 'ys': ys, 'scores': [score, score], 'left': xs, 'right': xs})
        gram.line(source=table, x='xs', y='ys', color='blue')
    
    return gram


# In[45]:


designate('histographing a histogram', 'histograms')

# function for constructing a histogram
def histograph(records, field, width=1):
    """Make a histogram from the dataset.
    
    Arguments:
        record: list of dicts, the records
        field: str, the field of interest
        width=1: int, the width of each histogram bar
        
    Returns:
        bokeh figure object
    """
    
    # gather up observations
    observations = [record[field] for record in records]
    
    # separate into bins
    bins, width, minimum, maximum = chop(observations, width)
    
    # get the counts and edges of each bin
    counts, edges = histogram(observations, bins=bins, range=(minimum, maximum))
    
    # get the zoom coordinates
    horizontal, vertical = zoom(observations, counts, width)

    # get the normal distribution, defaulting to a small deviation in case of zero
    mean = average(observations)
    deviation = max([0.000001, std(observations)])
    
    # begin histogram
    gram = draft(field, horizontal, vertical, mean, deviation)
    
    # block in bars on the histogram
    gram = block(gram, counts, edges, mean, deviation)
    
    # draw in equivalent normal distribution
    gram = normalize(gram, counts, edges, mean, deviation)
    
    return gram


# ### Assembling Associations

# Because the two sets of measurements were not taken concurrently, there must be some criteria to determine when measurements from one protocol correspond to measurements from the other protocol.  The strategy here is to use a Plateaued Gaussian weighing function that determines how strongly to weigh the association, based on the following parameters:
#     
# - distance: the distance in kilometers between measurements that will be granted full weight.
#     
# - interval: the time interval in days between measurements that will be granted full weight.
#     
# - lag: the time in days to anticipate an effect on mosquitoes from a secondary measurement some 
#     days before.
#     
# - confidence: the weight to grant a measurement twice the distance or interval. This determines how steeply the weighting shrinks as the intervals are surpassed.  A high confidence will grant higher weights to data outside the intervals.  A confidence of zero will have no tolerance for data slightly passed the interval.
#     
# - cutoff: the minimum weight to consider in the dataset.  A cutoff of 0.1, for instance, will only retain data if the weight is at least 0.1.
#     
# - inclusion: the maximum number of nearest secondary measurements to include for each mosquitoes measurement.
# 
# The sketch below shows several plateaus differing in their confidence parameter.

# In[46]:


designate('weighing function', 'weighting')

# weigh a pair of records according to the space and time between them
def weigh(space, time, settings):
    """Weigh the significance of a correlation based on the space and time between them
    
    Arguments:
        space: float, space in distance
        time: float, the time time in interval
        settings: dict
        
    Returns:
        float, the weight
    """
    
    # unpack settings
    distance, interval, lag = settings['distance'], settings['interval'], settings['lag']
    confidence, cutoff = settings['confidence'], settings['cutoff']
    
    # set default weight and factors to 1.0
    weight = 1.0
    factor = 1.0
    factorii = 1.0
    
    # if beyond the space, calculate the gaussian factor
    if abs(space) > distance:
        
        # default factor to 0, but calculate gaussian
        factor = 0.0
        if confidence > 0:
        
            # calculate the gaussian factor (e ^ -a d^2 = c), a = -ln (c) / d^2
            alpha = -log(confidence) / distance ** 2
            factor = exp(-alpha * (abs(space) - distance) ** 2)
            
    # if beyond time time, calculate gaussian factor
    if abs(time - lag) > interval:
        
        # default factor to 0, but calculate gaussian
        factorii = 0.0
        if confidence > 0:
            
            # calculate the gaussian factor (e ^ -a d^2 = c), a = -ln (c) / d^2
            beta = -log(confidence) / interval ** 2
            factorii = exp(-beta * (abs(time - lag) - interval) ** 2)
            
    # multiply by factors and apply cutoff
    weight *= (factor * factorii)
    weight *= int(weight >= cutoff)
    
    return weight


# In[47]:


designate('sketching weight function')

# set up faux settings
faux = {'distance': 10, 'interval': 1, 'lag': 0, 'confidence': 0.0, 'cutoff': 0.1}
fauxii = {'distance': 10, 'interval': 1, 'lag': 0, 'confidence': 0.5, 'cutoff': 0.1}
fauxiii = {'distance': 10, 'interval': 1, 'lag': 0, 'confidence': 0.8, 'cutoff': 0.1}

# make functions
weighting = lambda x: weigh(10, x, faux)
weightingii = lambda x: weigh(10, x, fauxii)
weightingiii = lambda x: weigh(10, x, fauxiii)

# make legend
legend = ['{}'.format(scheme['confidence']) for scheme in [faux, fauxii, fauxiii]]

# sketch
sketch(weighting, weightingii, weightingiii, legend=legend)


# All the secondary measurements are accumulated in a Balltree structure that automatically places measurements close together in the tree that are close together spatially and timewise.  The tree can then be queried with the time and geolocation information from a mosquito record to find the closest secondary measurements.  The weight is calculated for each secondary measurement, and if the weight is greater than the cutoff it is included in the dataset.

# Note that potentially several secondary measurements may be associated with a single mosquitoes measurement.  This has the effect of creating more regression points than mosquito records used.  Because statistical significance tends to increase with increasing number of samples, the p-value calculation of the potential for sampling bias is performed with reference to only the number of mosquito records used.

# In[48]:


designate('navigation buttons')

# set two navigation buttons
navigate()


# In[49]:


designate('plotting plateaus', 'weighting')

# function to plot a plateau
def plate(field, settings):
    """Plot a plateau weighting function.
    
    Arguments:
        field: str
        settings: dict
        
    Returns:
        bokeh object
    """
    
    # set offset parameter if an interval plot
    offset = settings['lag']

    # create functions
    functions = {'distance': lambda x: weigh(x, offset, settings), 'interval': lambda x: weigh(0, x, settings)}
    
    # calculate point range
    start = (settings[field] * -5) + offset
    finish = (settings[field] * 5) + offset
    total = finish - start
    chunk = total / 1000
    
    # calculate points
    xs = [start + (number * chunk) for number in range(1001)]
    ys = [functions[field](x) for x in xs]
    table = ColumnDataSource({'xs': xs, 'ys': ys})
    
    # create annotations
    annotations = [('{}'.format(field + units[field](feature)), '@xs')]
    annotations += [('weight', '@ys')]

    # create title
    title = 'Plateaued Guassian based on {} of {} {}'.format(field, settings[field], units[field](feature))
    if field == interval:
        
        # add delay
        title += ' with a {} day lag'.format(offset)
    
    # create parameters dictionary for guassian
    parameters = {}
    parameters['title'] = title
    parameters['x_axis_label'] = field
    parameters['y_axis_label'] = 'weight'
    parameters['plot_height'] = 300
    parameters['plot_width'] = 450

    # make graph
    plateau = figure(**parameters)
    plateau.line(source=table, x='xs', y='ys', line_width=1)

    # activate the hover tool
    hover = HoverTool(tooltips=annotations)
    plateau.add_tools(hover)
    
    return plateau


# In[50]:


designate('haversine', 'weighting')

# haversine to calculate distance in kilometers from latitude longitudes
def haversine(latitude, longitude, latitudeii, longitudeii):
    """Use the haversine function to calculate a distance from latitudes and longitudes.
    
    Arguments:
        latitude: float, latitude of first location
        longitude: float, longitude of first location
        latitudeii: float, latitude of second location
        longitudeii: float, longitude of second location
        
    Returns:
        float, the distance
    """
    
    # radius of the earth at the equator in kilometers
    radius = 6378.137
    
    # get distance from memory
    reference = (latitude, longitude, latitudeii, longitudeii)
    distance = memory.setdefault(reference, None)
    if distance is None:
    
        # convert to radians
        latitude = latitude * pi / 180
        latitudeii = latitudeii * pi / 180
        longitude = longitude * pi / 180
        longitudeii = longitudeii * pi / 180

        # calculate distance with haversine formula
        # d = 2r arcsin (sqrt (sin^2((lat2 - lat1) /2) + cos(lat1)cos(lat2)sin^2((lon2 - lon1) / 2)))
        radicand = sin((latitudeii - latitude) / 2) ** 2
        radicand += cos(latitude) * cos(latitudeii) * sin((longitudeii - longitude) / 2) ** 2
        distance = 2 * radius * arcsin(sqrt(radicand))
        
        # add to memory
        memory[reference] = distance
    
    return distance


# In[51]:


designate('calendar for measuring days between times', 'weighting')

# function to measure number of days between datetimes
def calendar(time, timeii):
    """Measure number of days between two datetimes.
    
    Arguments:
        time: float, first timestamp
        timeii: float, second timestamp
        
    Returns:
        float, number of days
    """
    
    # find time delta
    delta = timeii - time
    
    # convert to days
    conversion = delta / (24 * 60 * 60)
    
    return conversion


# In[52]:


designate('measuring between records', 'weighting')

# make a matrix of geotime vectors
def measure(record, recordii):
    """Measure the distance and time interval between two records.
    
    Arguments:
        record: record dict
        recordii: record dict
        
    Returns:
        (float, float) tuple, the distance and interval
    """
    
    # unpack first record
    latitude = record['latitude']
    longitude = record['longitude']
    time = record['time'].timestamp()
    
    # unpack second record
    latitudeii = recordii['latitude']
    longitudeii = recordii['longitude']
    timeii = recordii['time'].timestamp()
    
    # calculate distance and interval
    distance = haversine(latitude, longitude, latitudeii, longitudeii)
    interval = calendar(time, timeii)
    
    return distance, interval


# In[53]:


designate('adjusting to euclidean points', 'assembling')

# function for adjusting a lat long vector to euclidean points
def euclidize(record, settings, lag=None):
    """Adjust a geotime vector to a euclidean point.
    
    Arguments:
        record: dict
        settings: dict
        lag=None: float, lag in days (overrides settings)
        
    Returns:
        euclidean vector
    """
    
    # unpack settings
    distance, interval = settings['distance'], settings['interval']
    
    # get lag value
    if not lag:
        
        # get from settings
        lag = settings['lag']
    
    # determine number of latitude chunks based on distance sensitivity
    latitude = haversine(record['latitude'], record['longitude'], 0, record['longitude']) / distance
    
    # determine number of longitude chunks based on distance sensitivity
    longitude = haversine(record['latitude'], record['longitude'], record['latitude'], 0) / distance
    
    # determine number of time chunks based on time sensitivity
    time = lag + calendar(0, record['time'].timestamp()) / interval
    
    # form euclidean vector
    euclidean = [latitude, longitude, time]
    
    return euclidean


# In[54]:


designate('planting the tree', 'assembling')

# plant a Balltree for closest sample retrieval
def plant(records, settings):
    """Plant a Balltree to organize records by space and time.
    
    Arguments:
        records: list of dicts
        settings: dict
    
    Returns:
        sklearn Balltree object
    """
    
    # form matrix from secondary records
    matrix = array([euclidize(record, settings) for record in records])

    # construct the balltree
    tree = BallTree(matrix, leaf_size=2, metric='euclidean')   
    
    return tree


# In[55]:


designate('querying tree', 'assembling')

# querying tree function to retrieve record indices of the closet records
def query(tree, records, settings):
    """Query the balltree for closest neighbors to each primary record.
    
    Arguments:
        tree: sklearn balltree, the tree to query
        records: list of records dict
        settings: dict
        
    Returns:
        list of list of dicts
    """
    
    # set number to not exceed number of samples
    number = min([settings['inclusion'], len(dataii)])
    
    # query the tree
    matrix = [euclidize(record, settings, 0) for record in records]
    results = tree.query(matrix, k=number, return_distance=False)
    
    # get associated records and sort by feature size
    neighbors = [[dataii[int(entry)] for entry in indices] for indices in results]
    for cluster in neighbors:
        
        # sort by feature size
        cluster.sort(key=lambda record: record[feature], reverse=True)
    
    return neighbors


# In[56]:


designate('webbing together associations', 'assembling')

# form associations between records
def web(settings):
    """Web together samples in the second dataset with those of the first based on parameters.
    
    Arguments:
        settings: dict
        
    Returns:
        list of tuples, the associations
    """
    
    # plant a Balltree based on euclidean distances
    tree = plant(dataii, settings)
    
    # get all the nearest neighbors for each primary record
    neighbors = query(tree, data, settings)

    # assemble associations
    associations = []
    for record, cluster in zip(data, neighbors):

        # begin association
        location = (record['latitude'], record['longitude'])
        association = {'record': record, 'associates': [], 'location': location}
        for neighbor in cluster:

            # weigh the records, only keeping those with a 1% confidence
            space, time = measure(record, neighbor)            
            weight = weigh(space, time, settings)
            if weight > 0.0:

                # add to association
                association['associates'].append({'record': neighbor, 'weight': weight})

        # append if nonzero
        if len(association['associates']) > 0:
            
            # sort associates by weight
            association['associates'].sort(key=lambda associate: associate['weight'], reverse=True)
            associations.append(association)

    # sort by highest weight
    associations.sort(key=lambda association: association['associates'][0]['weight'], reverse=True)
    
    # add reference number
    [association.update({'reference': index}) for index, association in enumerate(associations)]
    
    return associations


# In[57]:


designate('summarizing associations in a table', 'assembling')

# function to summarize associations into a data table
def summarize(associations):
    """Summarize association information into a data table.
    
    Arguments:
        associations: list of dicts
        
    Returns:
        panda data frame
    """

    # make a table from associations
    summary = []

    # begin headers
    base = ['pair', 'point', 'weight', 'distance', 'days']
    headers = ['larvae', 'genus', 'date', 'latitude', 'longitude']
    headersii = [feature, 'date', 'latitude', 'longitude']
    parameters = [setting + '_setting' + units[setting](feature) for setting in settings.keys()]

    # get remaining headers
    remainder = [key for key in associations[0]['record'].keys() if key not in headers]
    remainderii = [key for key in associations[0]['associates'][0]['record'].keys() if key not in headersii]
    remainder.sort(key=lambda field: len(field))
    remainderii.sort(key=lambda field: len(field))

    # go through each association
    for index, association in enumerate(associations):

        # go through each associate
        record = association['record']
        for number, pair in enumerate(association['associates']):

            # begin row
            associate = pair['record']
            weight = pair['weight']
            row = [(association['reference'], number), (round(associate[feature], 2), round(record['larvae']))]
            row += [round(weight, 2)]
            row += [haversine(record['latitude'], record['longitude'], associate['latitude'], associate['longitude'])]
            row += [calendar(record['time'].timestamp(), associate['time'].timestamp())]

            # add primary record fields, secondary record fields, and parameters
            row += [record[field] for field in headers]
            row += [associate[field] for field in headersii]
            row += [value for value in settings.values()]

            # add all remaining primary and secondary record fields
            row += [record[field] for field in remainder]
            row += [associate[field] for field in remainderii]

            # add row to table
            summary.append(row)

    # construct labels, adding prefix in the case of duplicates
    labels = base + headers + headersii + parameters + remainder + remainderii
    duplicating = lambda index, label: '2nd_' + label if label in labels[:index] else label
    labels = [duplicating(index, label) for index, label in enumerate(labels)]
    
    # create dataframe from data
    summary = pandas.DataFrame.from_records(summary, columns=labels)
    
    return summary


# In[58]:


designate('subsampling by genus', 'assembling')

# subset the associations for a particular genus
def subsample(associations, genus):
    """Sub sample the associations based on a particular genus.
    
    Arguments:
        associations: list of dicts
        genus: str, the genus name
        
    Returns:
        list of dicts
    """
    
    # take subsample
    subsample = []
    for association in associations:
        
        # check the genus
        if genus == 'All' or genus == association['record']['genus']:
            
            # add to subsample
            subsample.append(association)
            
    return subsample


# In[59]:


designate('assembling samples', 'assembling')

# assemble samples from association routine
def assemble(associations):
    """Assemble samples from associations.
    
    Arguments:
        associations: list of dicts
        
    Returns:
        list of dicts
    """

    # form samples
    samples = []
    for association in associations:

        # go through each match
        record = association['record']
        for index, associate in enumerate(association['associates']):
            
            # make sample
            larvae = record['larvae']
            measurement = associate['record'][feature]
            weight = associate['weight']
            genus = record['genus']
            sample = {'y': larvae, 'x': measurement, 'weight': weight, 'genus': genus}
            
            # add other record info
            sample.update({'latitude': record['latitude'], 'longitude': record['longitude']})
            sample.update({'site': record['siteName'], 'organization': record['organizationName']})
            sample.update({'time': str(record['time'])})
            
            # add color and size components
            sample['color'] = indicators[genus]
            sample['size'] = int(1 + 20 * weight)
            
            # add indices to list
            sample['pair'] = (association['reference'], index)
            
            # add to list
            samples.append(sample)
            
    # sort
    samples.sort(key=lambda sample: sample['x'])
    
    return samples


# ### Table of GLOBE Countries

# The follow is a list of all GLOBE supporting countries and their country codes.

# In[60]:


designate('extracting all GLOBE countries')

# retrieve list of GLOBE countries and country codes from the API
url = 'https://api.globe.gov/search/dev/country/all/'
request = requests.get(url)
raw = json.loads(request.text)
countries = [record for record in raw['results']]

# create codes reference for later
codes = {record['countryName']: record['id'] for record in countries}
countries = {record['id']: record['countryName'] for record in countries}

# make table
table = [item for item in codes.items()]
table.sort(key=lambda item: item[0])

# print table
print('{} GLOBE countries:'.format(len(table)))
table


# ### Setting the Parameters

# Set the main geographic and time parameters in the box below.  You may specify a country name or a country code, with the code overriding the name when both are given.  Refer to the Table of GLOBE Countries above to see all countries involved in the GLOBE program.  Misspelled names and codes will be matched to the closest options available.  If both code and country name are left blank, it will default to all countries.  Also, the beginning date will default to Earth Day 1995 if left blank, and the ending date will default to the current date if left blank.  
# 
# Set the name of the secondary protocol to use as the independent variable of the study.  A list of the protocols in the mosquito habitat mapper bundle are listed for reference.  A close match to the name is generally sufficient.
# 
# Set the name of the particular feature of interest.  Currently this notebook only supports numerical features, so choose a feature with numerical values as opposed to categorical names. (For example, a field like 'mosquitohabitatmapperWaterSource' is not appropriate for the regression analyses in this notebook because the values are discrete source names instead of a continuum of values).  It may be difficult to know the name of this feature offhand.  In this case, you may use the Peruse button to see some sample records.  A close match to the name should be sufficient.
# 
# Also set the name of the time of measurement field, as this may vary depending on the protocol.  Usually 'measured' is sufficient to find the right one.  
# 
# You may click the Apply button to verify your choices.  Clicking the Propagate button will propagate the changes to the rest of the notebook.  Clicking Both will both apply the new parameters and propagate the changes throughout the notebook.

# In[61]:


designate('table of secondary protocols')

# get from protocols file
with open('protocols.txt', 'r') as pointer:
    
    # get all protocols
    protocols = [protocol.strip('\n') for protocol in pointer.readlines()]
    protocols = [protocol for protocol in protocols if 'X' in protocol]
    protocols = [protocol.strip('X').strip() for protocol in protocols]

# print as list
print('{} mosquito bundle protocols:'.format(len(protocols)))
protocols


# In[62]:


designate('setting the country, date range, and secondary protocol', 'settings')

# set the country name, defaulting to All countries if blank
country = 'United States'

# set the country code, defaulting to country name if left blank and overriding otherwise
code = ''

# set beginning date in 'YYYY-mm-dd' format, defaulting to 1995-04-22
beginning = '2016-01-01'

# set ending date in 'YYYY-mm-dd' format, defaulting to today's date
ending = ''

# set secondary protocol
secondary = 'dissolvedoxygen'

# set secondary protocol feature
feature = 'dissolvedoxygensDissolvedOxygenViaKitMgl'

# set secondary protocal measured time field ('measured' is usually close enough)
measured = 'measured'


# In[63]:


designate('applying setting changes or propagating throughout notebook')

# propagate changes
propagate('### Setting the Parameters', '### Retrieving Records from the API', '### Thank You!')


# In[64]:


designate('resolving user settings')

# define primary protocol name and larvae field
mosquitoes = 'mosquito_habitat_mapper'
larvae = 'mosquitohabitatmapperLarvaeCount'

# resolve country and code to default values
country, code = resolve(country, code)

# default beginning to first day of GLOBE and ending to current date if unspecified
beginning = beginning or '1995-04-22'
ending = ending or str(datetime.now().date())

# make api call to get number of records
print('\nprimary protocol: {}'.format(mosquitoes))
print('checking number of records...')
raw = call(mosquitoes, code, beginning, ending, sample=True)
count = raw['count']
print('{} {} records from {} ({}), from {} to {}'.format(count, mosquitoes, country, code, beginning, ending))

# infer closest match
perusal = [0]
secondary = infer(secondary, protocols)
print('\nsecondary protocol: {}'.format(secondary))

# make the API call and get the examples and record count
print('checking number of records...')
raw = call(secondary, code, beginning, ending, sample=True)
examples = [record for record in raw['results']]
count = raw['count']
print('{} records for {} from {} ({}), {} to {}'.format(count, secondary, country, code, beginning, ending))

# assert there must be more records
assert count > 0, '* Error * Unfortunately no records were returned for that protocol.'

# infer best matches
fields = [key for key in examples[0]['data'].keys()]
feature = infer(feature, fields)
measured = infer(measured, fields)

# print for inspection
print('\nsecondary feature: {}'.format(feature))
print('secondary time field: {}'.format(measured))


# In[65]:


designate('perusing secondary records')

# peruse through the ten sample records
def peruse(_):
    """Peruse through example records.
    
    Arguments:
        None
        
    Returns:
        None
    """
    
    # refresh this cell
    refresh(0)
    
    # get last number
    last = perusal[-1]
    
    # advance
    after = last + 1
    if after > 9:
        
        # return to beginning
        after = 0
        
    # append to numberings
    perusal.append(after)

    # print record
    print('record {} of 10:'.format(last))
    print(json.dumps(examples[last]['data'], indent=1))
    
    return None

# create button
button = Button(description="Peruse")
display(button)
button.on_click(peruse)


# In[66]:


designate('navigation buttons')

# set two navigation buttons
navigate()


# ### Retrieving Records from the API

# Click Retrieve to retrieve the data from the GLOBE API.  Clicking Propagate afterward will recalculate the notebook with the new dataset.

# In[67]:


designate('applying setting changes or propagating throughout notebook')

# propagate button
labels = ['Retrieve', 'Propagate', 'Both']
propagate('### Retrieving Records from the API', '### Pruning Outliers', '### Thank You!', labels)


# In[68]:


designate('retrieving and processing records from the api')

# get the mosquitoes records from the 'results' field, and prune off nulls
print('\nmaking api request...')
raw = call(mosquitoes, code, beginning, ending)
count = len(raw['results'])
results = [record for record in raw['results'] if record['data'][larvae] is not None]
results = process(results, primary=True)
formats = (len(results), mosquitoes, count, country, beginning, ending)
print('{} valid {} records (of {} total) from {}, {} to {}'.format(*formats))

# get the secondary protocol records and prune off nulls
print('\nmaking api request...')
raw = call(secondary, code, beginning, ending)
count = len(raw['results'])
resultsii = [record for record in raw['results'] if record['data'][feature] is not None]

# try to process data, may encounter error
try:

    # process data
    resultsii = process(resultsii, primary=False)
    formats = (len(resultsii), secondary, count, country, beginning, ending)
    print('{} valid {} records (of {} total) from {}, {} to {}'.format(*formats))
    
# but if trouble
except ValueError:
    
    # raise error
    message = '* Error! * Having trouble processing the data.  Is the requested field a numberical one?'
    raise Exception(message)

    
# raise assertion error if zero records
assert len(results) > 0, '* Error! * No valid {} records in the specified range'.format(mosquitoes)
assert len(resultsii) > 0, '* Error! * No valid {} records in the specified range'.format(secondary)


# In[69]:


designate('navigation buttons')

# set two navigation buttons
navigate()


# ### Pruning Outliers

# You may wish to remove datapoints that seem suspiciously like outliers.  In normally distributed data, ove 99% of the observations fall within 3 standard deviations.  Even in data not normally distributed, observations many standard deviations away from the mean are likely to be outliers.  Set the outlier thresholds below.  Observations will be removed from the study unless they fall within threshold number of standard deviations.

# In[70]:


designate('setting z-score thresholds', 'settings')

# set z-score thresholds, the number of standard deviations allowed
threshold = 20
thresholdii = 20


# In[71]:


designate('applying setting changes or propagating throughout notebook')

# propagate button
propagate('### Pruning Outliers', '### Filtering Records', '### Thank You!')


# In[72]:


designate('pruning away outliers')

# add default score field
for record in results + resultsii:
    
    # add default score field
    record['score'] = 1.0

# prune data
authentics, outliers = prune(results, 'larvae', threshold)
authenticsii, outliersii = prune(resultsii, feature, thresholdii)

# report
zipper = zip((authentics, authenticsii), (outliers, outliersii), (mosquitoes, secondary), ('larvae', feature))
for records, prunes, protocol, field in zipper:
    
    # report each outlier
    print('\n\nz-score  ' + field)
    for outlier in prunes:

        # print
        print(round(outlier['score'], 5), outlier[field])

    # report total
    print('\n{} observations removed'.format(len(prunes)))
    print('{} {} records after removing outliers'.format(len(records), protocol))


# In[73]:


designate('drawing histograms')

# construct histograms
gram = histograph(authentics, 'larvae', width=5)
gramii = histograph(authenticsii, feature, width=1)

# display plots
output_notebook()
show(Row(gram, gramii))


# In[74]:


designate('navigation buttons')

# set two navigation buttons
navigate()


# ### Filtering Records

# You may further filter the data if desired.  Smaller datasets render more quickly, for instance.  Set the criteria and click the Apply button to perform the search.  Clicking Propagate will propagate the changes down the notebook.  You may set a parameter to None to ignore any filtering.

# In[75]:


designate('setting filter parameters', 'settings')

# set the specific genera of interest ['Anopheles', 'Aedes', 'Culex', 'Unknown', 'Other']
# None defaults to all genera
genera = ['Anopheles', 'Aedes', 'Culex', 'Unknown', 'Other']

# set fewest and most larvae counts or leave as None
fewest = None
most = None

# set the inital or final date ranges (in 'YYYY-mm-dd' format), or leave as None
initial = None
final = None

# set the latitude boundaries, or leave as None
south = None
north = None

# set the longitude boundaries, or leave as None
west = None
east = None


# In[76]:


designate('applying setting changes or propagating throughout notebook')

# propagate changes
propagate('### Filtering Records', '### Defining the Weighting Scheme', '### Thank You!')


# In[77]:


designate('filtering records')

# set records to data
records = [record for record in authentics]
recordsii = [record for record in authenticsii]

# make parameters and fields list
parameters = [genera, fewest, most, initial, final, south, north, west, east]
fields = ['genus', 'larvae', 'larvae', 'date', 'date', 'latitude', 'latitude', 'longitude', 'longitude']

# make comparison functions list
within = lambda value, setting: value in setting
after = lambda value, setting: value >= datetime.strptime(str(setting), "%Y-%m-%d").date()
before = lambda value, setting: value <= datetime.strptime(str(setting), "%Y-%m-%d").date()
greater = lambda value, setting: value >= setting
lesser = lambda value, setting: value <= setting

# make associated functions
functions = [within, greater, lesser, after, before, greater, lesser, greater, lesser]
symbols = ['in', '>', '<', '>', '<', '>', '<', '>', '<']

# filter primaries
data, criteria = sift(authentics, parameters, fields, functions, symbols)
formats = (len(data), len(authentics), mosquitoes, criteria)
print('\n{} of {} {} records meeting criteria:\n\n{}'.format(*formats))

# filter secondaries
dataii, criteria = sift(authenticsii, parameters, fields, functions, symbols)
formats = (len(dataii), len(authenticsii), secondary, criteria)
print('\n{} of {} {} records meeting criteria:\n\n{}'.format(*formats))

# set genera to classification by default
genera = genera or classification


# In[78]:


designate('navigation buttons')

# set two navigation buttons
navigate()


# ### Defining the Weighting Scheme

# Because the two sets of measurements were not taken concurrently, there must be some criteria to determine when measurements from one protocol correspond to measurements from the other protocol.  The strategy here is to use a Plateaued Gaussian weighting function that determines how strongly to weigh the association, based on the following parameters:
#     
# - distance: the distance in kilometers between measurements that will be granted full weight.
#     
# - interval: the time interval in days between measurements that will be granted full weight.
#     
# - lag: the time in days to anticipate an effect on mosquitoes from a secondary measurement some 
#     days before.
#     
# - confidence: the weight to grant a measurement twice the distance or interval. This determines how steeply the weighting shrinks as the intervals are surpassed.  A high confidence will grant higher weights to data outside the intervals.  A confidence of zero will have no tolerance for data slightly passed the interval.
#     
# - cutoff: the minimum weight to consider in the dataset.  A cutoff of 0.1, for instance, will only retain data if the weight is at least 0.1.
#     
# - inclusion: the maximum number of nearest secondary measurements to include for each mosquitos measurement.
# 
# If you have tried the optimizer at the bottom of this notebook, you may use the Remember button to bring up a list of those results.

# In[79]:


designate('setting the weighting scheme', 'settings')

# set the distance sensitivity in kilometers
distance = 50

# set time interval sensitivity in days
interval = 1

# set the expected time lag in days
lag = 0

# weight for twice the interval
confidence = 0.8

# minimum weight to include
cutoff = 0.1

# number of nearest neighboring points to include
inclusion = 5


# In[80]:


designate('remembering optimization paramters button')

# reset parameters function
def remember(_):
    """Remeber previous obtimization results.
    
    Arguments:
        None
        
    Returns:
        None
    """
    
    # refresh
    refresh(0)
    
    # view last panda
    try:
        
        # view last panda
        display(optimizations[-1])
    
    # otherwise
    except IndexError:
        
        # message
        print('no optimizations generated yet')
    
    return None

# create button
button = Button(description="Remember")
display(button)
button.on_click(remember)


# In[81]:


designate('applying setting changes or propagating throughout notebook')

# propagate changes
propagate('### Defining the Weighting Scheme', '### Viewing the Data Table', '### Thank You!')


# In[82]:


designate('plotting plateaus')

# create settings dictionary
settings = {'distance': distance, 'interval': interval, 'lag': lag, 'confidence': confidence}
settings.update({'cutoff': cutoff, 'inclusion': inclusion})

# create plateaus
plateau = plate('distance', settings)
plateauii = plate('interval', settings)

# display plots
output_notebook()
show(Row(plateau, plateauii))


# The histograms below show the distribution of weights amongst the data set, and the distribution of secondary-measurement pairs found.

# In[83]:


designate('assembling associations')

# make associations
print('\nassembling associations...')
associations = web(settings)
coverage = round(len(associations) / len(data), 2)
print('{} {} records used, {} of {} records pulled'.format(len(associations), mosquitoes, coverage, len(data)))

# raise assertion error
assert len(associations) > 1, '* Error! * Not enough records retrieved for regression' 

# make summary table
summary = summarize(associations)

# construct histograms
records = [{'weight': association['associates'][0]['weight']} for association in associations]
gram = histograph(records, 'weight', width=0.1)

# construct histogram
records = [{'pairs': len(association['associates'])} for association in associations]
gramii = histograph(records, 'pairs', width=1)

# display
output_notebook()
show(Row([gram, gramii]))


# In[84]:


designate('navigation buttons')

# set two navigation buttons
navigate()


# ### Viewing the Data Table

# The data table of associations may be viewed below by clicking the View button.  Each row in the table begins with a pair of indices, with the first representing the primary mosquito record and the second representing the seconday record.  Each primary record may have multiple secondary records.  The strength of the association is given in the weights column, followed by the distance and time interval between them.  Thereafter are all the fields of both the primary and secondary records.

# In[85]:


designate('viewing data table button')

# function to examine record
def view(_):
    """Examine secondary protocol record.
    
    Arguments:
        None
        
    Returns:
        None
    """

    # display in output
    print('displaying...')
    display(summary)
    
    return None

# create button
button = Button(description="View")
button.on_click(view)
display(button)


# In[86]:


designate('navigation buttons')

# set two navigation buttons
navigate()


# ### Exporting Data to CSV

# It may be desirable to export the data to a csv file.  Click the Export button to export the data.  You will get a link to download the csv file.

# In[87]:


designate('exporting data to csv button')

# create push button linked to output
exporter = Output()
button = Button(description='Export')
display(button, exporter)

# function to export to zip file
def export(_):
    """Export the data as a csv file.
    
    Arguments:
        None
        
    Returns:
        None
    """
    
    #write dataframe to file
    name = 'mosquitoes_' + secondary + '_' + str(int(datetime.now().timestamp())) + '.csv'
    summary.to_csv(name)  
    
    # make link
    link = FileLink(name)
    
    # add to output
    exporter.append_display_data(link)

    return None

# add button click command
button.on_click(export)


# In[88]:


designate('navigation buttons')

# set two navigation buttons
navigate()


# ### Visualizing on a Map

# Click the map button to generate a map of all sampling locations.

# In[89]:


designate('viewing the map button')

# reset parameters function
def illustrate(_):
    """Illustrate sample locations in a map.
    
    Arguments:
        None
        
    Returns:
        None
    """
    
    # clear output
    execute('### Visualizing on a Map', '### Thank You!')
    
    # construct the map
    chart = construct()
    
    # add the markers
    chart = populate(chart)
    
    # add the controls
    chart = enhance(chart)
    
    # display the chart
    display(chart)
    
    return None

# create button
button = Button(description="Map")
display(button)
button.on_click(illustrate)


# The map is broken into 4 layers which may be selected from the Layers box underneath the full screen control on the left side of the map.  The layers may be toggled on and off here.  They are described as follows:

# 1) associated primaries: this layer contains markers for all mosquito habitat observations that have been associated with secondary protocol measurements.  The markers are colored according to genus as indicated by the map legend in the upper right.  The size of the marker reflects the size of the larvae count.  The opacity of the marker reflects the highest weight amongst secondary associations.  Clicking the marker will bring up a summary table and highlight the associated secondary measurements

# 2) associated secondaries: this layer contains markers for all secondary protocol observations that have been associated with mosquito measurements.  The markers are colored according the scale displayed in the lower right corner of the map.  The opacity of the marker reflects the highest weight amongst mosquito associations.  Clicking the marker will bring up a summary table and highlight the associated mosquito measurements.  Where multiple measurements have occured at the same longitude, latitude coordinates, the marker with the highest weight is displayed.

# 3) unassociated primaries: this layer contains markers for all mosquito habitat observations that have not been associated with secondary protocol measurements.  The markers are colored tan.  The size of the marker reflects the size of the larvae count.  Clicking the marker will bring up a summary table.

# 4) unassociated secondaries: this layer contains markers for all secondary protocol observations that have not been associated with mosquito measurements.  The markers are colored according the scale displayed in the lower right corner of the map.  Clicking the marker will bring up a summary table.  Where multiple measurements have occured at the same longitude, latitude coordinates, the marker with the highest secondary measurement is displayed.

# In[90]:


designate('navigation buttons')

# set two navigation buttons
navigate()


# In[91]:


designate('finding point in polygon', 'map')

# finding a polygon within
def within(record, polygon):
    """Determine if a point is within a polygon.
    
    Arguments:
        record: dict with latitude and longitude
        polygon: list of points
        
    Returns:
        boolean, point within polygon?
    """
    
    # set default to false
    answer = False
    
    # retrieve point
    point = (record['longitude'], record['latitude'])
    
    # form lines from polygon
    lines = [(first, second) for first, second in zip(polygon[:-1], polygon[1:])]

    # deterimine if line extended from point will cross the line
    crossings = 0
    for first, second in lines:
        
        # avoid zero divisions
        try:
        
            # determine vertical at point's horizontal
            slope = (second[1] - first[1]) / (second[0] - first[0])
            intercept = first[1] - slope * first[0]
            vertical = slope * point[0] + intercept
            
            # check if vertical is above the point and within the line
            if vertical > point[1]:
                
                # check for within bounds of line
                if vertical >= min([first[1], second[1]]) and vertical <= max([first[1], second[1]]):

                    # add a crossing
                    crossings += 1
                
        # check for zero
        except ZeroDivisionError:
            
            # pass
            pass
            
    # deterimine if odd or even number of crossings
    if crossings % 2 > 0:
        
        # answer is true
        answer = True
        
    return answer


# In[92]:


designate('chiseling a range into even numbers', 'map')

# chisel subroutine to round numbers of a range
def chisel(low, high):
    """Chisel a rounded range from a precise range.
    
    Arguments:
        low: float, low point in range
        high: float, high point in range
        
    Returns:
        (float, float) tuple, the rounded range
    """
    
    # find the mean of the two values
    mean = (low + high) / 2
    
    # find the order of magnitude of the mean
    magnitude = int(log10(mean))
    
    # get first and second 
    first = int(low / 10 ** magnitude)
    second = int(high / 10 ** magnitude) + 1
    
    # get all divisions
    divisions = [(10 ** magnitude) * number for number in range(first, second + 1)]
    
    # make half division if length is low
    if len(divisions) < 5:
        
        # create halves
        halves = [(entry + entryii) /2 for entry, entryii in zip(divisions[:-1], divisions[1:])]
        divisions += halves
        
    # sort
    divisions.sort()
    
    return divisions


# In[93]:


designate('mixing colors', 'map')

# mix colors according to a gradient
def mix(measurement, bracket, spectrum):
    """Mix the color for the measurment according to measurment bracket and color spectrum.
    
    Arguments:
        measurement: float, the measurement
        bracket: tuple of floats, low and high measurement range
        specturm: tuple of int tuples, the low and high rgb color endpoints
        
    Returns:
        str, hexadecimal color
    """
    
    # truncate measurement to lowest of highest of range
    low, high = bracket
    measurement = max([measurement, low])
    measurement = min([measurement, high])
    
    # determine fraction of range
    fraction = (measurement - low) / (high - low)
    
    # define mixing function
    mixing = lambda index: int(spectrum[0][index] + fraction * (spectrum[1][index] - spectrum[0][index]))
    
    # mix colors
    red, green, blue = [mixing(index) for index in range(3)]
    
    # define encoding function to get last two digits of hexadecimal encoding
    encoding = lambda intensity: hex(intensity).replace('x', '0')[-2:]
    
    # make the specific hue
    color = '#' + encoding(red) + encoding(green) + encoding(blue)

    return color


# In[94]:


designate('shrinking records to unique locations', 'map')

# shrink the secondary dataset to unique members per location
def shrink(records, criterion):
    """Shrink the records set to the unique record at each latitude longitude, based on a criterion field.
    
    Arguments:
        records: list of dicts
        criterion: str, field to sort by
        
    Returns:
        list of dicts
    """
    
    # add each record to a geocoordinates dictionary
    locations = {}
    for record in records:
        
        # each record is a contender for the maximum criterion
        contender = record
        location = (record['latitude'], record['longitude'])
        highest = locations.setdefault(location, None)
        if highest:
            
            # replace with record higher in the criterion
            contenders = [highest, contender]
            contenders.sort(key=lambda entry: entry[criterion], reverse=True)
            contender = contenders[0]
            
        # replace with condender
        locations[location] = contender
        
    # get the shrunken list
    uniques = [member for member in locations.values()]
    
    return uniques


# In[95]:


designate('code for stipling map with samples', 'map')

# stiple function to add a circle marker layer to the map
def stipple(records, weights, field, fill, line, thickness, radius, opacity, click, name=''):
    """Stipple the map with a layer of markers.
    
    Arguments:
        records: list of dicts, the records
        weights: list of floats, the weights
        field: str, the name of the appropriate field
        fill: function object, to determine fill color
        line: function object, to determine line color
        thickness: int, circle outline thickness
        radius: function object, to determine radius
        opacity: function object, to determine opacity as a function of weight
        click: function object, to act upon a click
        name: str, name of layer
        
    Returns:
        pyleaflet layer object
    """

    # create marker layer
    markers = []
    for record, weight in zip(records, weights):

        # unpack record
        latitude = record['latitude']
        longitude = record['longitude']
        date = record['date']
        measurement = record[field]
        
        # make circle marker
        circle = CircleMarker()
        circle.location = (latitude, longitude)
        
        # marker outline attributes
        circle.weight = thickness
        circle.color = line(record)
        circle.radius = radius(record)

        # set fill color attributes
        circle.fill = True
        circle.fill_opacity = opacity(weight)
        circle.fill_color = fill(record)

        # add click function
        circle.on_click(click)
        
        # annotate marker with popup label
        formats = (round(weight, 3), date, round(latitude, 3), round(longitude, 3), field, int(measurement))
        message = 'Weight: {}, Date: {}, Latitude: {}, Longitude: {}, {}: {}'.format(*formats)
        circle.popup = HTML(message)

        # add to markers layer
        markers.append(circle)

    # make marker layer
    layer = LayerGroup(layers=markers, name=name)
    
    return layer


# In[96]:


designate('fetching closest record subroutine', 'map')

def fetch(associations, latitude, longitude):
    """Fetch the closest record from the list of records based on latitude and longitude
    
    Arguments:
        associations: list of dicts
        latitude: float, latitude coordinate
        longitude: float, longitude coordinate
        
    Returns:
        dict, the closest record
    """
    
    # calculate the distances to all records
    distances = []
    for association in associations:
        
        # calculate squared distance
        geocoordinates = association['location']
        distance = (latitude - geocoordinates[0]) ** 2 + (longitude - geocoordinates[1]) ** 2
        distances.append((association, distance))
        
    # sort by distance and choose closest
    distances.sort(key=lambda pair: pair[1])
    closest = distances[0][0]
    
    return closest


# In[97]:


designate('exhibiting secondary markers on map subroutine', 'map')

def exhibit(coordinates, chart, associations, field, fill, line, thickness, radius, opacity, click):
    """Exhibit a record's associated records.
    
    Arguments:
        coordinates: (float, float) tuple, the latitude and longitude
        chart: ipyleaflets Map object
        associations: list of dicts
        field: str, the name of the appropriate field
        fill: function object, to determine fill color
        line: function object, to determine line color
        thickness: int, circle outline thickness
        radius: function object, to determine radius
        opacity: function object, to determine opacity as a function of weight
        click: function object, to act upon a click

    Returns:
        None
    """
    
    # remove last set of associates
    chart.layers = chart.layers[:5]
    
    # fetch the index of the closest association
    association = fetch(associations, *coordinates)
    
    # create marker layer
    records = [associate['record'] for associate in association['associates']]
    weights = [associate['weight'] for associate in association['associates']]
    layer = stipple(records, weights, field, fill, line, thickness, radius, opacity, click)
    
    # add layer
    chart.add_layer(layer)
    
    return None


# In[98]:


designate('constructing map', 'map')

# begin the map at a central latitude and longitude
def construct():
    """Construct the map at a central geolocation.
    
    Arguments:
        None
        
    Returns:
        ipyleaflet map object
    """

    # print status
    print('constructing map...')

    # get central latitude
    latitudes = [record['latitude'] for record in data]
    latitude = (max(latitudes) + min(latitudes)) / 2

    # get central longitude
    longitudes = [record['longitude'] for record in data]
    longitude = (max(longitudes) + min(longitudes)) / 2

    # set up map with topographical basemap zoomed in on center
    chart = Map(basemap=basemaps.Esri.WorldTopoMap, center=(latitude, longitude), zoom=5)
    
    return chart


# In[99]:


designate('populating map with markers', 'map')

# populate map with markers
def populate(chart):
    """Populate the chart with markers.
    
    Arguments:
        chart: ipyleaflet map
        
    Returns:
        ipyleaflet map
    """

    # create unassociated secondary marker layer
    print('marking unassociated secondaries...')
    indices = [association['record']['index'] for association in mirror]
    records = [record for record in uniques if record['index'] not in indices]
    weights = [0.0 for record in records]
    parameters = [records, weights, fieldii, fillii, lineii, thicknessii, radiusii]
    parameters += [empty, nothing, 'unassociated secondaries']
    layer = stipple(*parameters)
    chart.add_layer(layer)

    # create unassociated primaries layer
    print('marking unassociated primaries...')
    indices = [association['record']['index'] for association in associations]
    records = [record for record in data if record['index'] not in indices]
    weights = [0.0 for record in records]
    parameters = [records, weights, field, tan, tan, thickness, radius]
    parameters += [empty, nothing, 'unassociated primaries']
    layer = stipple(*parameters)
    chart.add_layer(layer)

    # create associated secondary marker layer
    print('marking associated secondaries...')
    records = [association['record'] for association in heavies]
    weights = [association['associates'][0]['weight'] for association in heavies]
    parametersii = [chart, mirror, 'larvae', fill, black, thickness, radius, highlight, nothing]
    clickingii = lambda **event: exhibit(event['coordinates'], *parametersii)
    parameters = [records, weights, fieldii, fillii, lineii, thicknessii, radiusii, opacityii]
    parameters += [clickingii, 'associated secondaries']
    layer = stipple(*parameters)
    chart.add_layer(layer)

    # create primary marker layer
    print('marking associated primaries...')
    records = [association['record'] for association in associations]
    weights = [association['associates'][0]['weight'] for association in associations]
    parametersii = [chart, associations, feature, fillii, black, thicknessii, radiusii, highlight, nothing]
    clicking = lambda **event: exhibit(event['coordinates'], *parametersii)
    parameters = [records, weights, field, fill, line, thickness, radius, opacity]
    parameters += [clicking, 'associated primaries']
    layer = stipple(*parameters)
    chart.add_layer(layer)
    
    return chart


# In[100]:


designate('enhancing map with controls', 'map')

# enhance the map with control
def enhance(chart):
    """Enhance the map with controls and legends.
    
    Arguments:
        chart: ipyleaflet map object
        
    Returns:
        ipyleaflet map object
    """

    # add full screen button and map scale
    chart.add_control(FullScreenControl())
    chart.add_control(ScaleControl(position='topright'))

    # add genus legend
    labels = [Label(value = r'\(\color{' + 'black' +'} {' + 'Genera:'  + '}\)')]
    labels += [Label(value = r'\(\color{' + indicators[genus] +'} {' + genus  + '}\)') for genus in classification]
    legend = VBox(labels)

    # send to output
    out = Output(layout={'border': '1px solid blue', 'transparency': '50%'})
    with out:

        # display
        display(legend)

    # add to map
    control = WidgetControl(widget=out, position='topright')
    chart.add_control(control)

    # add colormap legend
    colors = [mix(division, bracket, spectrum) for division in divisions]
    colormap = StepColormap(colors, divisions, vmin=bracket[0], vmax=bracket[1], caption=feature)
    out = Output(layout={'border': '1px solid blue', 'transparency': '80%', 'height': '50px', 'overflow': 'scroll'})
    text = Label(value = r'\(\color{' + 'blue' +'} {' + feature  + '}\)')
    with out:

        # display
        display(text, colormap)

    # add to map
    control = WidgetControl(widget=out, position='bottomright')
    chart.add_control(control)

    # add layers control
    control = LayersControl(position='topleft')
    chart.add_control(control)
    
    return chart


# In[101]:


designate('making mirror of associations')

# go through primary record
mirror = {}
for association in associations:
    
    # and each set of associated secondary records
    record = association['record']
    for associate in association['associates']:
         
        # skip if weight is zero 
        weight = associate['weight']
        if associate['weight'] > 0.0:
            
            # add default entry
            index = record['index']
            if index not in mirror.keys():
                
                # begin an entry
                location = (associate['record']['latitude'], associate['record']['longitude'])
                entry = {'record': associate['record'], 'associates': [], 'location': location}
                mirror[index] = entry
                
            # populate entry and sort
            mirror[index]['associates'].append({'record': record, 'weight': weight})
            mirror[index]['associates'].sort(key=lambda associate: associate['weight'], reverse=True)
            
# make into list and sort
mirror = [value for value in mirror.values()]
mirror.sort(key=lambda association: association['associates'][0]['weight'], reverse=True)

# shrink to uniques
uniques = shrink(dataii, feature)


# In[102]:


designate('shrinking mirror to highest weight per location')

# construct records
unshrunk = []
for index, association in enumerate(mirror):
    
    # construct faux records
    record = association['record']
    faux = {'index': index, 'latitude': record['latitude'], 'longitude': record['longitude']}
    faux['weight'] = association['associates'][0]['weight']
    unshrunk.append(faux)
    
# shrink records
shrunken = shrink(unshrunk, 'weight')
indices = [record['index'] for record in shrunken]
heavies = [mirror[index] for index in indices]


# In[103]:


designate('describing markers')

# get 5th and 95th percentile bracket
measurements = [association['record'][feature] for association in mirror]
low = percentile(measurements, 5)
high = percentile(measurements, 95)
divisions = chisel(low, high)
bracket = (divisions[0], divisions[-1])

# define color spectrum as dark and light rgb tuples
dark = (100, 50, 150)
light = (0, 255, 255)
spectrum = (dark, light)

# define minimum and maximum of larvae counts
measurements = [association['record']['larvae'] for association in associations]
minimal = min(measurements)
maximal = max(measurements)

# create standin functions
black = lambda record: 'black'
tan = lambda record: 'tan'
yellow = lambda record: 'yellow'
nothing = lambda **event: None
empty = lambda weight: 0.0
highlight = lambda weight: 1.0

# create functions for primary markers
field = 'larvae'
fill = lambda record: indicators[record['genus']]
line = fill
thickness = 2
radius = lambda record: min([25, 5 + int(0.1 * record['larvae'])])
opacity = lambda weight: 0.1 + max([weight - 0.1, 0])

# create functions for secondary markers
fieldii = feature
fillii = lambda record: mix(record[feature], bracket, spectrum)
lineii = fillii
thicknessii = 2
radiusii = lambda record: 20
opacityii = lambda weight: 0.1 + max([weight - 0.1, 0])


# ### Thank You!

# Thanks for taking this notebook for a spin.  Please feal free to direct questions, issues, or other feedback to Matthew Bandel at matthew.bandel@ssaihq.com

# In[104]:


designate('navigation buttons')

# set two navigation buttons
navigate()

